import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Console from "../views/Console";
import Files from "@/views/Files";
import Settings from "@/views/Settings";
import store from "@/store"
import Auth from "@/views/Auth";


function checkAuth(next) {
  if (!store.getters.isAuthenticated) {
    next('/auth')
  } else {
    next()
  }
}

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter (to, from, next) {
      checkAuth(next)
    }
  },
  {
    path: '/console',
    name: 'Console',
    component: Console,
    beforeEnter (to, from, next) {
      checkAuth(next)
    }
  },
  {
    path: '/files',
    name: 'Files',
    component: Files,
    beforeEnter (to, from, next) {
      checkAuth(next)
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    beforeEnter (to, from, next) {
      checkAuth(next)
    }
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
