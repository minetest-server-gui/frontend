import { createStore } from 'vuex'
import { consoleModule } from "./modules/console";
import { serverModule } from "./modules/server";
import { notificationsModule } from "./modules/notifications";
import {authenticate, register, testJWT, isValidJwt, EventBus} from "@/utils";

const store = createStore({
  state: {
    darkMode: true,
    user: {},
    jwt: "",
  },
  mutations: {
    TOGGLE_DARK_MODE(state) {
      state.darkMode = !state.darkMode
    },

    SET_USER_DATA(state, payload) {
      console.log('SET_USER_DATA payload = ', payload)
      state.userData = payload.userData
    },
    SET_JWT_TOKEN(state, payload) {
      console.log('SET_JWT_TOKEN payload = ', payload)
      localStorage.token = payload.jwt.token
      state.jwt = payload.jwt
      console.log(state)
    }
  },
  actions: {
    toggleDarkMode({ commit }) {
      commit('TOGGLE_DARK_MODE')
    },

    login({ commit }, userData) {
      commit('SET_USER_DATA', { userData })
      return authenticate(userData)
          .then(response => commit('SET_JWT_TOKEN', { jwt: response.data }))
          .catch(error => {
            console.log('Error Authenticating: ', error)
            EventBus.$emit('failed-authentication', error)
          })
    },

    register({ commit, dispatch }, userData) {
      commit('SET_USER_DATA', { userData })
      return register(userData)
          .then(dispatch('login', userData))
          .catch(error => {
            console.log('Error Registering: ', error)
            EventBus.$emit('failed-registering', error)
          })
    },

    test({ state }) {
      console.log(state)
      return testJWT(state.jwt)
    },

  },
  getters: {
    isAuthenticated: state => {
      return isValidJwt(state.jwt.token)
    }
  },

  modules: {
    consoleStream: consoleModule,
    serverAPI: serverModule,
    notificationsModule: notificationsModule
  }
})

export default store