export const consoleModule = {
    namespaced: true,

    state: {
        console_stream: ''
    },

    mutations: {
        OUTPUT_STREAM(state, newLine) {
            // newLine = newLine.replace(/ /g, "&nbsp;")
            state.console_stream = '<samp>' + state.console_stream + newLine + '<br/>' + '</samp>'
        }
    },

    actions: {
        outputStream({ commit }, newLine) {
            commit('OUTPUT_STREAM', newLine)
        }
    },

    modules: {
    }
}
