export const notificationsModule = {
    namespaced: true,

    state: {
        notifications: []
    },

    mutations: {
        PUSH_NOTIFICATION(state, notification) {
            let id = (Math.random().toString(36) + Date.now().toString(36)).substr(2)
            state.notifications.push({
                ...notification,
                id: id
            })
        },

        REMOVE_NOTIFICATION(state, notificationToRemove) {
            state.notifications = state.notifications.filter(notification => {
                return notification.id !== notificationToRemove.id
            })
        }
    },

    actions: {
        addNotification({ commit }, notification) {
            commit('PUSH_NOTIFICATION', notification)
        },

        removeNotification({ commit }, notification) {
            commit('REMOVE_NOTIFICATION', notification)
        }
    },

    modules: {
    }
}
