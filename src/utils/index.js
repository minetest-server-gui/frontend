import axios from 'axios'
import {app} from "@/main";

export const EventBus = app

export function isValidJwt (jwt) {
    if (!jwt || jwt.split('.').length < 3) {
        return false
    }
    const data = JSON.parse(atob(jwt.split('.')[1]))
    const exp = new Date(data.exp * 1000) // because JS uses milliseconds
    const now = new Date()
    console.log(now < exp)
    return now < exp
}

export function testJWT (jwt) {
    return axios.get("http://localhost:5000/auth/test/", {
        headers: {
            Authorization: `Bearer: ${jwt.token}`,
        },
    })
}

export function authenticate (userData) {
    return axios.post("http://localhost:5000/auth/login/", userData)
}

export function register (userData) {
    return axios.post("http://localhost:5000/auth/register/", userData)
}
